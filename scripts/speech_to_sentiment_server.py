#!/usr/bin/env python

from speech_to_sentiment.srv import *
import rospy
import pyttsx#this is an API used to convert text to speech install it b 'sudo pip install pyttsx'
words1=["hello","good","happy","with pleasure","thank you"]
words2=["bad","sad","hell","mad"]
words3=["okay"]
rospy.init_node('speech_to_sentiment_server')#server node is initialized

def handle_text_to_sentiment(req):
   
    if req.a in words1:                             #req from client is compared against list
            print "Returning sentiment"
            engine=pyttsx.init()
            engine.say('you sound positive')       #text is converter into speech using pyttsx
            engine.runAndWait()
            return TextToSentimentResponse("you sound positive")
    elif req.a in words2:
            print "Returning sentiment"
            engine=pyttsx.init()
            engine.say('you sound negative')
            engine.runAndWait()
            return TextToSentimentResponse("you sound negative")
    elif req.a in words3:
            print "Returning sentiment"
            engine=pyttsx.init()
            engine.say('you sound neutral')
            engine.runAndWait()
            return TextToSentimentResponse("you sound neutral")

def text_to_sentiment_server():   
    print "Ready to check the sentiment."
    #a service is defined with name text_to_sentiment
    s = rospy.Service('text_to_sentiment', TextToSentiment, handle_text_to_sentiment)   
    rospy.spin()

if __name__ == "__main__":
    text_to_sentiment_server()

