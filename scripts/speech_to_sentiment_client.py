#!/usr/bin/env python
import sys
import rospy
import os
import speech_recognition as sr
from speech_to_sentiment.srv import *
rospy.init_node('speech_to_sentiment_client')#client node is initialized 
text_to_sentiment = rospy.ServiceProxy('text_to_sentiment', TextToSentiment)#service is defined with name text_to_sentiment and srv file name TextToSentiment

def speech_to_sentiment_client(x):
    rospy.wait_for_service('text_to_sentiment') 
    try: 
        resp1 = text_to_sentiment(x)
        return resp1.s
        rospy.spin()
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e
 

if __name__ == '__main__':
    try:
  #this command records the audio and saves it to speech_to_text file.it is a linux command        
        os.system("ffmpeg -f pulse -i default speech_to_text.wav") 
        print "Saysomething.."                                                                           
        AUDIO_FILE = ("speech_to_text.wav")
        # use the audio file as the audio source
        r = sr.Recognizer()
        with sr.AudioFile(AUDIO_FILE) as source:
    #reads the audio file. Here we use record instead of listen     #function
             audio = r.record(source)  
        try:
             x=r.recognize_google(audio)
             print "Google is recognising audio please wait...."
             print "your emotion for %s = %s"%(x, speech_to_sentiment_client(x))
        except sr.UnknownValueError:
             print "Google Speech Recognition could not understand audio"
             
        except sr.RequestError as e:
             print("Could not request results from Google Speech Recognition service; {0}".format(e))
    
    except rospy.ROSInterruptException:
        pass
  

