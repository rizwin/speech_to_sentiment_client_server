#!/usr/bin/env python

from speech_to_sentiment.srv import *
import rospy
words1=["hello","good","happy","with pleasure","thank you"]
words2=["bad","sad","hell","mad"]

def handle_text_to_sentiment(req):
    l=req
    if l in words1:
            print("you sound positive")
            print("Returning [%s]" % (req.a)
            return TextToSentimentResponse(req.a)
    elif l in words2:
            print("you sound negative")
            print("Returning [%s]" % (req.a)
            return TextToSentimentResponse(req.a)
    else:
            print("you sounded neutral")
            print("Returning [%s]" % (req.a)
            return TextToSentimentResponse(req.a)
   

def text_to_sentiment_server():
    rospy.init_node('speech_to_sentiment_server')
    s = rospy.Service('text_to_sentiment', TextToSentiment, handle_text_to_sentiment)
    print "Ready to check the sentiment."
    rospy.spin()

if __name__ == "__main__":
    text_to_sentiment_server()

